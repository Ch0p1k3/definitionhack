# Definition Hack

## Комманда "Папамс"

### Описание комманд discord-bot:



### Структура репозитория:

    .
    ├── README.md
    └── src
        ├── discord_client
        │   ├── grpc <--- Файлы сгенерированные для Python gRPC
        │   │   ├── __init__.py
        │   │   ├── service_pb2.py
        │   │   └── service_pb2_grpc.py
        │   └── impl <--- клиент дискорда на питоне
        │       ├── discord_client.py
        │       └── requirements.txt
        ├── go_server
        │   ├── go.mod
        │   ├── go.sum
        │   ├── go_grpc <--- Файлы сгенерированые для Golang gRPC
        │   │   ├── go.mod
        │   │   ├── go.sum
        │   │   ├── service.pb.go
        │   │   └── service_grpc.pb.go
        │   ├── main.go <--- Имплементация интерфейсов для gRPC
        │   ├── store <--- Хранилище в ОЗУ
        │   │   ├── local_store
        │   │   │   ├── store.go
        │   │   │   └── user_repository.go
        │   │   ├── repository.go
        │   │   └── store.go
        │   └── wallet_request <--- Пакет, где посылаются запросы на Chatex API
        │       ├── wallet_request.go
        │       └── wallet_request_test.go
        └── proto
            └── service.proto <--- Описание интерфейсов для gRPC

### Команды бота:

1. */RegisterUser token* - зарегестрироваться используя RefreshToken token. Никакие из следующих команд не выполнятся без регистрации.

2. */GetWalletCurrencies* - получить текущее состояние кошелька в chatex.

3. */GetInventory* - получить инвентарь вещей пользователя(мы после регистрации выдаем каждому 5 уникальных вещей)

4. */SellItem item_id currency_name currency_count* - выставить на продажу из вашего инвентаря(item_id) за количество currency_count криптовалюты currency_name

5. */GetSellingItems* - получить все вещи, которые в данный момент продаются в формате кортежей (seller_id, selling_item_id, currency_name, currency_count)

6. */BuyItem seller_id item_id currency_name currency_count* - купить у seller_id вещь item_id за его выставленные currency_name, currency_count
