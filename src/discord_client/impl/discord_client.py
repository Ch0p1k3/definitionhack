# bot.py
import os

import discord
from dotenv import load_dotenv
import sys 
from discord_components import Button, Select, SelectOption, ComponentsBot

sys.path.append(os.path.abspath('../grpc/'))

import service_pb2
import service_pb2_grpc
import grpc
from google.protobuf.json_format import ParseDict
from google.protobuf.json_format import MessageToDict, MessageToJson
from google.protobuf.empty_pb2 import Empty

from discord.ext import commands

load_dotenv()
TOKEN = 'ODkxMjU1NjU0NzEzNTMyNDI3.YU7sgg.fxNuCAar85fiOTNdAO1G0yWXt-I'

bot = commands.Bot(command_prefix='/')
client = discord.Client()

async def GetInventoryGRPC(memberId):
    protoUserMessage = {'memberId': memberId}
    protoUserMessage = ParseDict(protoUserMessage, service_pb2.User())
    
    async with grpc.aio.insecure_channel('localhost:5000') as channel:
        stubServer = service_pb2_grpc.PythonDiscordToGoServerChannelStub(channel)
        response = await stubServer.GetInventoryItems(protoUserMessage)
        return MessageToDict(response)['items']


@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')


@bot.command(name='GetInventory')
async def GetInventory(ctx):
    if ctx.author == client.user:
        return
    
    print(ctx.author, type(ctx.author), str(ctx.author))
    
    inventoryItems = await GetInventoryGRPC(str(ctx.author))
    print(inventoryItems)
    if len(inventoryItems) == 0:
        await ctx.send('У вас пустой инвентарь')
    else:
        await ctx.send('Ваши предметы:')
        sellItems = '' 
        for i in range(0, len(inventoryItems)):
            if inventoryItems[i] != {}:
                sellItems += str(i) + ': ' + '\t' + 'id= ' + str(inventoryItems[i]['id']) + '\n'
        await ctx.send(sellItems)


@bot.command(name='SellItem')
async def SellItem(ctx, itemId, currencyName, currencyCount):
    if ctx.author == client.user:
        return
    
    protoUserMessage = service_pb2.SellItemUserData(
        SellerMemberId=str(ctx.author),
        item=service_pb2.Item(
            id=int(itemId)
        ),
        price=service_pb2.Price(
            currencyName=currencyName,
            currencyCount=float(currencyCount)
        )
    )
    
    async with grpc.aio.insecure_channel('localhost:5000') as channel:
        stubServer = service_pb2_grpc.PythonDiscordToGoServerChannelStub(channel)
        response = await stubServer.SellItemUser(protoUserMessage)
    
    if MessageToDict(response)['error'] == 'Invalid Item':
        await ctx.send('Вы пытаетесь выставить вещь, которой у вас нет')
    elif MessageToDict(response)['error'] == 'Bad Auth':
        await ctx.send('Вы не зарегестрированы')
        await ctx.send('/RegisterUser <token>')
    else:
        await ctx.send('Вещь успешно выставлена на продажу')


@bot.command(name='RegisterUser')
async def RegisertUser(ctx, refreshToken):
    if ctx.author == client.user:
        return

    protoUserMessage = service_pb2.RefreshTokenData(
        memberId=str(ctx.author),
        refreshToken=refreshToken
    )
    
    async with grpc.aio.insecure_channel('localhost:5000') as channel:
        stubServer = service_pb2_grpc.PythonDiscordToGoServerChannelStub(channel)
        response = await stubServer.RegisterUser(protoUserMessage)
    
    if MessageToDict(response)['error'] != 'Ok':
        await ctx.send('Неверный токен')
    else:
        await ctx.send('Вы успешно зарегистрировались')


@bot.command(name='GetWalletCurrencies')
async def GetWalletCurrencies(ctx):
    if ctx.author == client.user:
        return

    protoUserMessage = service_pb2.User(memberId=str(ctx.author))
    
    async with grpc.aio.insecure_channel('localhost:5000') as channel:
        stubServer = service_pb2_grpc.PythonDiscordToGoServerChannelStub(channel)
        response = await stubServer.GetUsersCurrencyWallet(protoUserMessage)
    
    if MessageToDict(response)['error'] == 'Bad Auth':
        await ctx.send('Вы не зарегестрированы')
        await ctx.send('/RegisterUser <token>')
        return
    
    currencies = MessageToDict(response)['price']
    if len(currencies) == 0:
        await ctx.send('У вас отсутствует на кошельке криптовалюта')
    else:
        await ctx.send('Криптовалюта в вашем chatx кошельке:')
        for currency in currencies:
            await ctx.send(currency['currencyName'] + ': ' + str(currency['currencyCount']))


@bot.command(name='GetSellingItems')
async def GetSellingItems(ctx):
    if ctx.author == client.user:
        return

    protoUserMessage = service_pb2.Empty()

    async with grpc.aio.insecure_channel('localhost:5000') as channel:
        stubServer = service_pb2_grpc.PythonDiscordToGoServerChannelStub(channel)
        response = await stubServer.GetSellingItems(protoUserMessage)
    print(response)

    if response == []:
        await ctx.send('В данный момент никто не выставил вещей на продажу')
        return
    
    sellingItems = MessageToDict(response)['listSellItemUserData']
   
    if len(sellingItems) == 0:
        await ctx.send('В данный момент никто не выставил вещей на продажу')
    else:
        await ctx.send('Вещи, которые выставлены на продажу')
        for sellerData in sellingItems:
            userId = sellerData['SellerMemberId']
            itemId = sellerData['item']['id']
            currencyName = sellerData['price']['currencyName']
            currencyCount = sellerData['price']['currencyCount']
            await ctx.send(
                f"User id: {userId} продает item id: {itemId}" 
                f"за {currencyName}{currencyCount}"
            )


@bot.command(name='BuyItem')
async def BuyItem(ctx, sellerMemberId, itemId, currencyName, currencyCount, PIN):
    if ctx.author == client.user:
        return

    protoUserMessage = service_pb2.BuyItemUserData(
        BuyerMemberId=str(ctx.author),
        item=service_pb2.Item(
            id=int(itemId)
        ),
        price=service_pb2.Price(
            currencyName=currencyName,
            currencyCount=float(currencyCount)
        ),
        SellerMemberId=sellerMemberId,
        PIN=str(PIN)
    )
    
    async with grpc.aio.insecure_channel('localhost:5000') as channel:
        stubServer = service_pb2_grpc.PythonDiscordToGoServerChannelStub(channel)
        response = await stubServer.BuyItemUser(protoUserMessage)
   
    error = MessageToDict(response)['error']
    print(error)

    if error == 'Not enough amount':
        await ctx.send(f'На вашем кошельке недостаточно {currencyName}')
    elif error == 'Item not found':
        await ctx.send(f'Продавец {sellerMemberId} не продает itemId:{itemId}')
    elif error == 'Does Not Exist Coin':
        await ctx.send(f'На вашем кошельке нет {currencyName}')
    elif error == 'Bad auth':
        await ctx.send('Вы не зарегистрированы')
        await ctx.send('/RegisterUser <token>')
    elif error == 'Forbidden':
        await ctx.send('Forbidden transaction. Возможно проблемы с PIN')
    else:
        await ctx.send('Покупка произошла успешно')


@bot.command(name='Help')
async def Help(ctx):
    if ctx.author == client.user:
        return

    await ctx.send(
        f'Команды бота:\n'
        f'1. /RegisterUser token - зарегестрироваться используя RefreshToken token. Никакие из следующих команд не выполнятся без регистрации.\n'
        f'2. /GetWalletCurrencies - получить текущее состояние кошелька в chatex.\n'
        f'3. /GetInventory - получить инвентарь вещей пользователя(мы после регистрации выдаем каждому 5 уникальных вещей)\n'
        f'4. /SellItem item_id currency_name currency_count - выставить на продажу из вашего инвентаря(item_id) за количество currency_count криптовалюты currency_name\n'
        f'5. /GetSellingItems - получить все вещи, которые в данный момент продаются в формате кортежей (seller_id, selling_item_id, currency_name, currency_count)\n'
        f'6. /BuyItem seller_id item_id currency_name currency_count - купить у seller_id вещь item_id за его выставленные currency_name, currency_count\n'
    )

bot.run(TOKEN)
