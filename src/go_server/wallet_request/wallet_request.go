package wallet_request

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type AccessToken struct {
	Token   string `json:"access_token"`
	Expired uint64 `json:"expires_at"`
}

type CurrancyFromRequest struct {
	Coin     string `json:"coin"`
	Amount   string `json:"amount"`
	Held     string `json:"held"`
	IsHidden bool   `json:"is_hidden"`
}

type Currancy struct {
	Coin   string
	Amount float64
}

type InvoceData struct {
	Amount          string `json:"amount"`
	Coin            string `json:"coin"`
	CountryCode     string `json:"country_code"`
	Fiat            string `json:"fiat"`
	LangId          string `json:"lang_id"`
	PaymentSystemId int    `json:"payment_system_id"`
}

type TSecondFactor struct {
	Mode string `json:"mode"`
	Code string `json:"code"`
}

type TransferData struct {
	Amount       string        `json:"amount"`
	Coin         string        `json:"coin"`
	Recipient    string        `json:"recipient"`
	SecondFactor TSecondFactor `json:"second_factor"`
}

func GetAccessToken(refreshToken string) (*AccessToken, error) {
	h := &http.Header{}
	h.Set("Authorization", fmt.Sprintf("Bearer %s", refreshToken))

	bytesOfBody, err := sendRequestsAndGetBody("POST", "auth/access-token", h, nil)
	if err != nil {
		return nil, err
	}

	result := &AccessToken{}
	err = json.Unmarshal(bytesOfBody, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func GetWallet(token AccessToken) (*[]Currancy, error) {
	h := &http.Header{}
	h.Set("Authorization", fmt.Sprintf("Bearer %s", token.Token))

	bytesOfBody, err := sendRequestsAndGetBody("GET", "wallet", h, nil)
	if err != nil {
		return nil, err
	}

	resultWithTrash := []CurrancyFromRequest{}
	err = json.Unmarshal(bytesOfBody, &resultWithTrash)
	if err != nil {
		err = fmt.Errorf("%s", err.Error()+string(bytesOfBody))
		return nil, err
	}

	result := []Currancy{}
	for _, value := range resultWithTrash {
		amountConverted, err := strconv.ParseFloat(value.Amount, 64)
		if err != nil {
			return nil, err
		}
		if amountConverted != 0 {
			result = append(result, Currancy{
				value.Coin,
				amountConverted,
			})
		}
	}

	return &result, nil
}

func GetProfile(token AccessToken) (*map[string]interface{}, error) {
	h := &http.Header{}
	h.Set("Authorization", fmt.Sprintf("Bearer %s", token.Token))

	bytesOfBody, err := sendRequestsAndGetBody("GET", "me", h, nil)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = json.Unmarshal(bytesOfBody, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func GetNickname(token AccessToken) (*string, error) {
	profile, err := GetProfile(token)
	if err != nil {
		return nil, err
	}
	if value, ok := (*profile)["login"]; ok {
		result := fmt.Sprintf("%v", value)
		return &result, nil
	}
	return nil, fmt.Errorf("No login in GetProfile")
}

func DetectCountry(token AccessToken) (*map[string]interface{}, error) {
	h := &http.Header{}
	h.Set("Authorization", fmt.Sprintf("Bearer %s", token.Token))

	bytesOfBody, err := sendRequestsAndGetBody("GET", "countries/detect", h, nil)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = json.Unmarshal(bytesOfBody, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func CreateInvoice(token AccessToken, coin string, amount string) (*map[string]interface{}, error) {
	h := &http.Header{}
	h.Set("Authorization", fmt.Sprintf("Bearer %s", token.Token))
	h.Set("Content-type", "application/json")

	invoceData := InvoceData{}
	profilePoint, err := GetProfile(token)
	if err != nil {
		return nil, err
	}
	profile := *profilePoint
	if val, ok := profile["country_code"]; ok {
		invoceData.CountryCode = fmt.Sprintf("%v", val)
	} else {
		return nil, fmt.Errorf("GetProfile does not consist country_code")
	}
	if val, ok := profile["lang_id"]; ok {
		invoceData.LangId = fmt.Sprintf("%v", val)
	} else {
		return nil, fmt.Errorf("GetProfile does not consist lang_id")
	}
	invoceData.Amount = amount
	invoceData.PaymentSystemId = 2
	invoceData.Fiat = "RUB"
	invoceData.Coin = coin
	js, err := json.Marshal(invoceData)
	if err != nil {
		return nil, err
	}
	bytesOfBody, err := sendRequestsAndGetBody("POST", "invoices", h, &js)
	if err != nil {
		return nil, err
	}
	var result map[string]interface{}
	err = json.Unmarshal(bytesOfBody, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func Transfer(token AccessToken, coin string, amount string, recipient string, code string) (*map[string]interface{}, error) {
	h := &http.Header{}
	h.Set("Authorization", fmt.Sprintf("Bearer %s", token.Token))
	h.Set("Content-type", "application/json")

	transferData := TransferData{}
	transferData.Amount = amount
	transferData.Coin = coin
	transferData.Recipient = recipient
	transferData.SecondFactor.Mode = "PIN"
	transferData.SecondFactor.Code = code
	js, err := json.Marshal(transferData)
	if err != nil {
		return nil, err
	}
	bytesOfBody, err := sendRequestsAndGetBody("POST", "wallet/transfers", h, &js)
	if err != nil {
		return nil, err
	}
	var result map[string]interface{}
	err = json.Unmarshal(bytesOfBody, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func sendRequestsAndGetBody(method string, action string, headers *http.Header, body *[]byte) ([]byte, error) {
	client := &http.Client{}
	var req *http.Request = nil
	if body != nil {
		ht, err := http.NewRequest(method, "https://api.staging.iserverbot.ru/v1/"+action, strings.NewReader(string(*body)))
		if err != nil {
			return nil, err
		}
		req = ht
	} else {
		ht, err := http.NewRequest(method, "https://api.staging.iserverbot.ru/v1/"+action, nil)
		if err != nil {
			return nil, err
		}
		req = ht
	}

	req.Header = *headers

	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if response.Status != "200 OK" && response.Status != "201 Created" {
		return nil, fmt.Errorf("HTTTP STATUS: %s", response.Status)
	}

	bytesOfBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return bytesOfBody, nil
}
