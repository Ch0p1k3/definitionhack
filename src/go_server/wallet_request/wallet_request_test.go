package wallet_request

import (
	"os"
	"testing"
)

func TestGetAccessToken(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")

	if token == "" {
		t.Skip("No env")
	}
	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Logf("%s", accessToken.Token)
}

func TestGetWallet(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")
	if token == "" {
		t.Skip("No env")
	}

	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	result, err := GetWallet(*accessToken)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(result)
}

func TestGetProfile(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")
	if token == "" {
		t.Skip("No env")
	}

	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	result, err := GetProfile(*accessToken)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(result)
}

func TestGetNickname(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")
	if token == "" {
		t.Skip("No env")
	}

	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	result, err := GetNickname(*accessToken)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(*result)
}

func TestCreateInvoice(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")
	if token == "" {
		t.Skip("No env")
	}

	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	// site := "https://www.google.com/"
	result, err := CreateInvoice(*accessToken, "BTC", "0.0001")
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(result)
}

func TestDetectCountry(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")
	if token == "" {
		t.Skip("No env")
	}

	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	result, err := DetectCountry(*accessToken)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(result)
}

func TestTransfer(t *testing.T) {
	token := os.Getenv("REFRESH_TOKEN")
	if token == "" {
		t.Skip("No env REFRESH_TOKEN")
	}
	recipient := os.Getenv("RECIPIENT")
	if recipient == "" {
		t.Skip("No env RECIPIENT")
	}
	pin := os.Getenv("PIN_FOR_TRANSFER")
	if pin == "" {
		t.Skip("No env PIN_FOR_TRANSFER")
	}
	accessToken, err := GetAccessToken(token)
	if err != nil {
		t.Fatal(err.Error())
	}
	result, err := Transfer(*accessToken, "BTC", "0.0001", recipient, pin)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(result)
}
