module definitionhack/src/go_server/go_grpc

go 1.16

require (
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
