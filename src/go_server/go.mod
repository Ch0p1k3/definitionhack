module github.com/definitionhack/src/server

go 1.16

replace definitionhack/src/go_server/go_grpc => ./go_grpc

require (
	definitionhack/src/go_server/go_grpc v0.0.0-00010101000000-000000000000
	google.golang.org/grpc v1.41.0
)
