package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"

	"github.com/definitionhack/src/server/store"
	"github.com/definitionhack/src/server/store/local_store"
	"github.com/definitionhack/src/server/wallet_request"

	mes_grpc "definitionhack/src/go_server/go_grpc"

	"google.golang.org/grpc"
)

type GPRCServer struct {
	storeServer store.Store
	mes_grpc.UnimplementedPythonDiscordToGoServerChannelServer
}

func (s *GPRCServer) AccessWithApi(userId string, accessToken *wallet_request.AccessToken) error {
	return s.storeServer.GetAuthData().InitAccess(userId, accessToken.Token)
}

func (s *GPRCServer) RefreshAccessToken(userId string) error {
	refreshToken, err := s.storeServer.GetAuthData().GetRefresh(userId)
	if err != nil {
		return err
	}
	accessToken, err := wallet_request.GetAccessToken(*refreshToken)
	s.storeServer.GetAuthData().InitAccess(userId, accessToken.Token)
	return nil
}

func (s *GPRCServer) CheckAuth(userId string) bool {
	_, err1 := s.storeServer.GetAuthData().GetAccess(userId)
	_, err2 := s.storeServer.GetAuthData().GetRefresh(userId)
	if err1 != nil || err2 != nil {
		return false
	}
	return true
}

var counterTest int

func (s *GPRCServer) CreateTestItems(in *mes_grpc.RefreshTokenData) {
	for i := 0; i < 5; i++ {
		s.storeServer.GetUserData().Insert(in.MemberId, &mes_grpc.Item{Id: uint64(counterTest + i)})
	}
	counterTest += 5
}

func (s *GPRCServer) RegisterUser(ctx context.Context, in *mes_grpc.RefreshTokenData) (*mes_grpc.OptionalError, error) {
	s.storeServer.GetAuthData().InitRefresh(in.MemberId, in.RefreshToken)
	accessToken, err := wallet_request.GetAccessToken(in.RefreshToken)
	s.CreateTestItems(in)
	if err != nil {
		invlalidAccessToken := "InvlalidAccessToken"
		return &mes_grpc.OptionalError{Error: &invlalidAccessToken}, nil
	}
	if err := s.AccessWithApi(in.MemberId, accessToken); err != nil {
		refreshResp := "Invalid refresh token"
		return &mes_grpc.OptionalError{Error: &refreshResp}, nil
	} else {
		okResp := "Ok"
		return &mes_grpc.OptionalError{Error: &okResp}, nil
	}
}

func (s *GPRCServer) GetInventoryItems(ctx context.Context, usr *mes_grpc.User) (*mes_grpc.Inventory, error) {
	return s.storeServer.GetUserData().GetInventory(usr.MemberId), nil
}

func (s *GPRCServer) BuyItemUser(ctx context.Context, itemUsr *mes_grpc.BuyItemUserData) (*mes_grpc.OptionalError, error) {
	log.Println(itemUsr.BuyerMemberId, itemUsr.Item.Id, itemUsr.SellerMemberId, itemUsr.PIN, itemUsr)
	if !s.CheckAuth(itemUsr.BuyerMemberId) {
		BadAuth := "Bad Auth"
		log.Println("Bad Auth")
		return &mes_grpc.OptionalError{Error: &BadAuth}, nil
	}
	_, err := s.storeServer.GetUserData().FindItem(itemUsr.SellerMemberId, itemUsr.Item.Id)

	if err != nil {
		BadAccessToInventory := "Item not found"
		log.Println("Item not found")
		return &mes_grpc.OptionalError{Error: &BadAccessToInventory}, nil
	}
	val := s.storeServer.GetSellersData().FindInSellers(itemUsr.Item.Id)
	if val == nil {
		ItemNotFount := "Item not found"
		return &mes_grpc.OptionalError{Error: &ItemNotFount}, nil
	}
	AccessToken, err := s.storeServer.GetAuthData().GetAccess(itemUsr.BuyerMemberId)
	WalletReq, _ := wallet_request.GetWallet(wallet_request.AccessToken{Token: *AccessToken})
	s.storeServer.GetUserData().RefreshWallet(itemUsr.BuyerMemberId, WalletReq)
	wallet, err := s.storeServer.GetUserData().GetWallet(itemUsr.BuyerMemberId)
	for _, it := range *wallet {
		if strings.ToLower(it.Coin) == strings.ToLower(itemUsr.Price.CurrencyName) {
			if it.Amount < float64(itemUsr.Price.CurrencyCount) {
				NotEnoughtAmount := "Not enought amount"
				log.Println("Not enought amount")
				return &mes_grpc.OptionalError{Error: &NotEnoughtAmount}, nil
			} else {
				accessToken, err := s.storeServer.GetAuthData().GetAccess(itemUsr.BuyerMemberId)
				if err != nil {
					BadToken := "Bad token"
					log.Println("Bad token")
					return &mes_grpc.OptionalError{Error: &BadToken}, nil
				}
				accessTokenSeller, err := s.storeServer.GetAuthData().GetAccess(itemUsr.SellerMemberId)
				nick, err := wallet_request.GetNickname(wallet_request.AccessToken{Token: *accessTokenSeller})
				if err != nil {
					NoUser := "No user"
					log.Println("No user")
					return &mes_grpc.OptionalError{Error: &NoUser}, nil
				}
				deb, err := wallet_request.Transfer(
					wallet_request.AccessToken{Token: *accessToken},
					itemUsr.Price.CurrencyName,
					strconv.FormatFloat(float64(itemUsr.Price.CurrencyCount), 'g', 1, 64),
					*nick,
					itemUsr.PIN)
				log.Println(deb, err)
				log.Println("Succesful transfer")
				if err != nil {
					Forbidden := "Forbidden"
					log.Println("Forbdden")
					return &mes_grpc.OptionalError{Error: &Forbidden}, nil
				} else {
					Ok := "Ok"
					_, err := s.storeServer.GetUserData().EraseItem(itemUsr.SellerMemberId, itemUsr.Item.Id)
					if err == nil {
						if err2 := s.storeServer.GetUserData().Insert(itemUsr.BuyerMemberId, itemUsr.Item); err2 != nil {
							BadInsertToBuyer := "Bad Insert To Buyer"
							return &mes_grpc.OptionalError{Error: &BadInsertToBuyer}, nil
						}
						log.Println("Ok")
						return &mes_grpc.OptionalError{Error: &Ok}, nil
					} else {
						BadErase := "BadErase"
						log.Println("BadErase")
						return &mes_grpc.OptionalError{Error: &BadErase}, nil
					}

				}
			}
		}
	}
	/*
		accessToken, err := s.storeServer.GetAuthData().GetAccess(itemUsr.BuyerMemberId)
		if err != nil {
			BadToken := "Bad token"
			log.Println("Bad token")
			return &mes_grpc.OptionalError{Error: &BadToken}, nil
		}
		accessTokenSeller, err := s.storeServer.GetAuthData().GetAccess(itemUsr.SellerMemberId)

		nick, err := wallet_request.GetNickname(wallet_request.AccessToken{Token: *accessTokenSeller})
		if err != nil {
			NoUser := "No user"
			log.Println("No user")
			return &mes_grpc.OptionalError{Error: &NoUser}, nil
		}
		deb, err := wallet_request.Transfer(
			wallet_request.AccessToken{Token: *accessToken},
			itemUsr.Price.CurrencyName,
			strconv.FormatFloat(float64(itemUsr.Price.CurrencyCount), 'g', 1, 64),
			*nick,
			itemUsr.PIN)
		log.Println(deb, err)
	*/
	DoesNotExistCoin := "Does Not Exist Coin"
	return &mes_grpc.OptionalError{Error: &DoesNotExistCoin}, nil
}

func (s *GPRCServer) GetUsersCurrencyWallet(ctx context.Context, in *mes_grpc.User) (*mes_grpc.PricesOrError, error) {
	if !s.CheckAuth(in.MemberId) {
		BadAuth := "Bad Auth"
		return &mes_grpc.PricesOrError{Error: &BadAuth}, nil
	}
	s.RefreshAccessToken(in.MemberId)
	AccessToken, err := s.storeServer.GetAuthData().GetAccess(in.MemberId)
	WalletReq, _ := wallet_request.GetWallet(wallet_request.AccessToken{Token: *AccessToken})
	s.storeServer.GetUserData().RefreshWallet(in.MemberId, WalletReq)
	wallet, err := s.storeServer.GetUserData().GetWallet(in.MemberId)
	if err != nil {
		BadAccessToWallet := "BadAccessToWallet" // NEED tests

		return &mes_grpc.PricesOrError{Price: nil, Error: &BadAccessToWallet}, nil
		//wallet, err := s.storeServer.GetUserData().GetWallet(in.MemberId)
	}
	prices := make([]*mes_grpc.Price, 0)
	for _, val := range *wallet {
		prices = append(prices, &mes_grpc.Price{CurrencyName: val.Coin, CurrencyCount: float32(val.Amount)}) // fix
	}
	Ok := "Ok"
	return &mes_grpc.PricesOrError{Price: prices, Error: &Ok}, nil
}

func (s *GPRCServer) SellItemUser(ctx context.Context, sellItemData *mes_grpc.SellItemUserData) (*mes_grpc.OptionalError, error) {
	s.RefreshAccessToken(sellItemData.SellerMemberId)
	if !s.CheckAuth(sellItemData.SellerMemberId) {
		BadAuth := "Bad Auth"
		return &mes_grpc.OptionalError{Error: &BadAuth}, nil
	}
	if _, err := s.storeServer.GetUserData().FindItem(sellItemData.SellerMemberId, sellItemData.Item.Id); err != nil {
		a := "Invalid Item"
		log.Printf("bad item error in SellItemUser")
		return &mes_grpc.OptionalError{Error: &a}, nil
	} else {
		err := s.storeServer.GetSellersData().Insert(sellItemData)
		if err != nil {
			log.Println("bad insert")
			badInsert := "Bad insert"
			return &mes_grpc.OptionalError{Error: &badInsert}, nil
		}
		log.Printf("good")
		opt := "Ok"
		return &mes_grpc.OptionalError{Error: &opt}, nil
	}
}

func (s *GPRCServer) GetSellingItems(ctx context.Context, empty *mes_grpc.Empty) (*mes_grpc.SellingItems, error) { // fix
	return s.storeServer.GetSellersData().GetSellers(), nil
}

func CreateGPRCServer() *GPRCServer {
	return &GPRCServer{storeServer: local_store.New()}
}

func main() {
	serverAddr := os.Getenv("MESSENGER_SERVER_PORT")
	if serverAddr == "" {
		serverAddr = "5000"
		fmt.Println("Missing MESSENGER_SERVER_PORT variable, using default value: " + serverAddr)
	}

	s := grpc.NewServer()
	//GPRCserv := CreateGPRCServer()
	srv := CreateGPRCServer()

	counterTest = 0
	mes_grpc.RegisterPythonDiscordToGoServerChannelServer(s, srv)

	l, err := net.Listen("tcp", ":"+serverAddr)
	if err != nil {
		log.Fatal(err)
	}

	if err := s.Serve(l); err != nil {
		log.Fatal(err)
	}
}
