package store

import (
	"definitionhack/src/go_server/go_grpc"

	"github.com/definitionhack/src/server/wallet_request"
)

type UserRepository interface {
	Insert(usedID string, poll *go_grpc.Item) error
	GetInventory(id string) *go_grpc.Inventory
	FindItem(usedID string, id uint64) (*go_grpc.Item, error)
	GetWallet(userID string) (*[]wallet_request.Currancy, error)
	RefreshWallet(userID string, wallet *[]wallet_request.Currancy)
	EraseById(usedId string)
	EraseItem(userId string, id uint64) (go_grpc.Item, error)
}

type SellersRepository interface {
	Insert(data *go_grpc.SellItemUserData) error
	FindInSellers(uint64) *go_grpc.SellItemUserData
	GetSellers() *go_grpc.SellingItems
	EraseById(itemId uint64)
}

type AuthRepository interface {
	InitRefresh(userId string, refreshToken string) error
	InitAccess(userId string, accessToken string) error
	GetAccess(userId string) (*string, error)
	GetRefresh(userId string) (*string, error)
}
