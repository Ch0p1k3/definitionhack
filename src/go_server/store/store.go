package store

type Store interface {
	GetUserData() UserRepository
	GetSellersData() SellersRepository
	GetAuthData() AuthRepository
}
