package local_store

import (
	"definitionhack/src/go_server/go_grpc"
	"fmt"
	"sync"

	"github.com/definitionhack/src/server/store"
	"github.com/definitionhack/src/server/wallet_request"
)

type UserRepository struct {
	store      *store.Store
	data       map[string]*go_grpc.Inventory
	dataWallet *[]wallet_request.Currancy
	Mu         sync.RWMutex
}

func (r *UserRepository) EraseById(usedId string) {
	r.Mu.Lock()
	delete(r.data, usedId)
	r.Mu.Unlock()
}

func (r *UserRepository) RefreshWallet(userID string, wallet *[]wallet_request.Currancy) {
	r.dataWallet = wallet
}

func (r *UserRepository) GetWallet(userID string) (*[]wallet_request.Currancy, error) {
	if r.dataWallet != nil {
		return r.dataWallet, nil
	} else {
		return nil, fmt.Errorf("No wallet")
	}
}

func (r *UserRepository) Insert(usedID string, u *go_grpc.Item) error {
	r.Mu.Lock()
	if _, ok := r.data[usedID]; !ok {
		var inv go_grpc.Inventory
		inv.Items = make([]*go_grpc.Item, 0)
		r.data[usedID] = &inv
	}
	r.data[usedID].Items = append(r.data[usedID].Items, u)
	r.Mu.Unlock()
	return nil
}

func (r *UserRepository) GetInventory(usedID string) *go_grpc.Inventory {
	r.Mu.Lock()
	if val, ok := r.data[usedID]; ok {
		r.Mu.Unlock()
		return val
	} else {
		var inv go_grpc.Inventory
		inv.Items = make([]*go_grpc.Item, 0)
		r.Mu.Unlock()
		return &inv // not find
	}
}

func (r *UserRepository) FindItem(usedID string, id uint64) (*go_grpc.Item, error) {
	for _, val := range r.data[usedID].Items {
		if val.Id == id {
			return val, nil
		}
	}
	return nil, fmt.Errorf("No item with this id")
}

func (r *UserRepository) EraseItem(userId string, id uint64) (go_grpc.Item, error) {
	isGood := false
	items := r.data[userId].Items
	resp := go_grpc.Item{}
	for i, val := range items {
		if val.Id == id {
			resp = *val
			items = append(items[:i], items[i+1:]...)
			isGood = true
		}
	}
	if isGood {
		r.Mu.Lock()
		r.data[userId].Items = items
		r.Mu.Unlock()
		return resp, nil
	} else {
		return resp, fmt.Errorf("Bad userId or itemId")
	}
}

//-------------------------------------------------------
type SellersRepository struct {
	store   *store.Store
	dataMap map[uint64][]go_grpc.SellItemUserData
	data    []go_grpc.SellItemUserData
}

func (r *SellersRepository) FindInSellers(id uint64) *go_grpc.SellItemUserData {
	for i := 0; i < len(r.data); i++ {
		if id == r.data[i].Item.Id {
			return &go_grpc.SellItemUserData{
				SellerMemberId: r.data[i].SellerMemberId,
				Price:          r.data[i].Price,
				Item:           r.data[i].Item,
			}
		}
	}
	return nil
}

func (r *SellersRepository) Insert(userData *go_grpc.SellItemUserData) error {
	r.data = append(r.data, go_grpc.SellItemUserData{
		SellerMemberId: userData.SellerMemberId,
		Price:          userData.Price,
		Item:           userData.Item,
	})
	return nil
}

func (r *SellersRepository) GetSellers() *go_grpc.SellingItems {
	items := go_grpc.SellingItems{ListSellItemUserData: make([]*go_grpc.SellItemUserData, 0)}
	for i := 0; i < len(r.data); i++ {
		items.ListSellItemUserData = append(items.ListSellItemUserData, &go_grpc.SellItemUserData{
			SellerMemberId: r.data[i].SellerMemberId,
			Item:           r.data[i].Item,
			Price:          r.data[i].Price,
		})
	}
	return &items
}

func (r *SellersRepository) EraseById(itemId uint64) {
	var eraseId *int
	eraseId = nil
	for i := 0; i < len(r.data); i++ {
		if itemId == r.data[i].Item.Id {
			*eraseId = i
		}
	}
	r.data = append(r.data[:*eraseId], r.data[*eraseId+1:]...)
}

//-------------------------------------------------------

type Money struct {
	currencyName  string
	currencyCount float64
}

type AuthRepository struct {
	store       *store.Store
	dataAccess  map[string]*string
	dataRefresh map[string]*string
}

func (r *AuthRepository) InitRefresh(userId string, refreshToken string) error {
	r.dataRefresh[userId] = &refreshToken
	return nil
}

func (r *AuthRepository) InitAccess(usedId string, accessToken string) error {
	r.dataAccess[usedId] = &accessToken
	return nil
}

func (r *AuthRepository) GetAccess(userId string) (*string, error) {
	if val, ok := r.dataAccess[userId]; ok {
		return val, nil
	} else {
		return nil, fmt.Errorf("Bad access to dataAccess")
	}
}

func (r *AuthRepository) GetRefresh(userId string) (*string, error) {
	if val, ok := r.dataRefresh[userId]; ok {
		return val, nil
	} else {
		return nil, fmt.Errorf("Bad access to dataRefresh")
	}
}
