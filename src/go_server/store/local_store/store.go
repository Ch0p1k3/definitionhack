package local_store

import (
	"definitionhack/src/go_server/go_grpc"

	"github.com/definitionhack/src/server/store"
	"github.com/definitionhack/src/server/wallet_request"
)

type Store struct {
	userRepository    *UserRepository
	sellersRepository *SellersRepository
	authRepository    *AuthRepository
}

func New() *Store {
	return &Store{}
}

func (s *Store) GetUserData() store.UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}
	wallet := make([]wallet_request.Currancy, 0)
	s.userRepository = &UserRepository{dataWallet: &wallet, data: make(map[string]*go_grpc.Inventory)}
	return s.userRepository
}

func (s *Store) GetSellersData() store.SellersRepository {
	if s.sellersRepository != nil {
		return s.sellersRepository
	}

	s.sellersRepository = &SellersRepository{data: make([]go_grpc.SellItemUserData, 0)}
	return s.sellersRepository
}

func (s *Store) GetAuthData() store.AuthRepository {
	if s.authRepository != nil {
		return s.authRepository
	}
	s.authRepository = &AuthRepository{dataAccess: make(map[string]*string), dataRefresh: make(map[string]*string)}
	return s.authRepository
}
